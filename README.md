# **Customer Segmentation Analysis**

![](./resources/cs1.png)

### *Problem Statement*
##### A wide range of companies struggle to retain their flock of customers due to a variety of reasons (poor pricing, competition, inaccurate forecasting, etc). One main reason is that the companies dont understand (or care) who their customers are or what they are interested in.

### *Approach to Solution*
##### One way to reconcile the customer information "black box" is to group customers into discrete groups. This is called customer segmentation, or sometimes also called market segmentation. There are many segmentation schemes a company can use to appraoch these customer groups such as geographic, industry, product, and others. For this analysis we will have a focus on product.

### *Purpose*
##### This project is meant to serve as a high-level introduction  into customer segmentation analysis and R.

### *Clone Repo*
```
$ git clone https://gitlab.com/tyler.bailey2093/customer-segmentation-analysis.git
$ cd customer-segmentation-analysis
```